package com.nng.interview.homework.applicant.balintbiro.domain;

import com.nng.interview.homework.exercise.DoContains;
import com.nng.interview.homework.exercise.TreeNode;

/**
 * Created by Bálint on 2014.10.29..
 */
public class DoContainsImpl<T extends Comparable<T>> extends DoContains<T> {

    @Override
    public boolean contains(TreeNode<T> tTreeNode, TreeNode<T> tTreeNode2) {
        return false;
    }
}
